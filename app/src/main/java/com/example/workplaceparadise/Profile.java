package com.example.workplaceparadise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.microsoft.identity.client.IAuthenticationResult;

import org.w3c.dom.Text;

public class Profile extends AppCompatActivity {
    private static final String TAG = "Test";
    private final String photoAPICall = "https://graph.microsoft.com/v1.0/me/photo/$value";

    Button buttonLogout, buttonBookARoom;
    ImageView imageViewProfile;
    TextView textViewName, textViewEmail, textViewJobTitle, textViewOfficeLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();
        String[] data = intent.getStringExtra("DATA").split("\"");
        //Initialize
        buttonLogout = findViewById(R.id.button_Logout);
        buttonBookARoom = findViewById(R.id.buttonBookARoom);
        imageViewProfile = findViewById(R.id.imageView);
        textViewName = findViewById(R.id.textView_Name);
        textViewEmail = findViewById(R.id.textView_Email);
        textViewJobTitle = findViewById(R.id.textView_Jobtitle);
        textViewOfficeLocation = findViewById(R.id.textView_officeLocation);

        textViewName.setText(data[9]);
        textViewEmail.setText(data[21]);
        textViewJobTitle.setText(data[17]);
        textViewOfficeLocation.setText(data[27]);
        final String token = intent.getStringExtra("TOKEN");
        callMessageAPI(token);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogout = new Intent(Profile.this, MainActivity.class);
                startActivity(intentLogout);
            }
        });

        buttonBookARoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentShowRoom = new Intent(Profile.this, Listing.class);
                intentShowRoom.putExtra("TOKEN", token);
                startActivity(intentShowRoom);
            }
        });
    }

    private void callMessageAPI(final String token){
        Log.d(TAG, "Message API " + token);
        MSGraphRequestWrapper.callGraphAPIUsingVolleyforImage(Profile.this,
                photoAPICall,
                token,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        imageViewProfile.setImageBitmap(Bitmap.createScaledBitmap(response, 400, 500, false));
                        Log.d(TAG, "Response: " + response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error: " + error.toString());
                    }
                });
    }
}
