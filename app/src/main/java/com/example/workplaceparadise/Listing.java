package com.example.workplaceparadise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class Listing extends AppCompatActivity {
    private ArrayList<String> arrayList;
    private static final String TAG = "Test";
    private String roomListUrl = "https://graph.microsoft.com/beta/me/findRoomLists";
    ListView container;
    HashMap<String, String> map = new HashMap<>();
    Intent intentRooms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);
        setTitle("Buildings List");
        container = findViewById(R.id.rooms_container);
        arrayList = new ArrayList<>();

        final Intent intent = getIntent();
        final String token = intent.getStringExtra("TOKEN");
        callFindRoomListAPI(token);
        container.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                intentRooms = new Intent(Listing.this, RoomsList.class);
                intentRooms.putExtra("ITEM", arrayList.get(position));
                intentRooms.putExtra("MAP", map);
                intent.putExtra("TOKEN", token);
                startActivity(intentRooms);
            }
        });

    }

    private void callFindRoomListAPI(final String token) {
        Log.d(TAG, "Microsoft Identity Platform Access Token " + token);
        MSGraphRequestWrapper.callGraphAPIUsingVolley(Listing.this,
                roomListUrl,
                token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Response: " + response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("value");
                            Log.d(TAG, "Object: " + jsonArray.toString());
                            for (int i = 0; i < jsonArray.length();i++) {
                                JSONObject object = (JSONObject) jsonArray.get(i);
                                Log.d(TAG, "Object " + i + ":" + object.toString());
                                arrayList.add(object.getString("name"));
                                map.put(object.getString("name"), object.getString("address"));
                            }
                        }  catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            ArrayAdapter<String> roomAdapter = new ArrayAdapter<>(Listing.this, android.R.layout.simple_list_item_1, arrayList);
                            container.setAdapter(roomAdapter);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error: " + error.toString());
                    }
                });
    }
}
