package com.example.workplaceparadise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RoomsList extends AppCompatActivity {
    private static final String TAG = "Test";
    String roomsURL;
    ListView roomsListView;
    HashMap<String, String> map;
    ArrayList<String> roomsL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms_list);
        setTitle("Rooms List");
        roomsListView = findViewById(R.id.listviewRooms);
        Intent intent = getIntent();
        map = (HashMap<String, String>) intent.getSerializableExtra("MAP");
        String buildingName = intent.getStringExtra("ITEM");
        roomsL = new ArrayList<>();

        roomsURL = "https://graph.microsoft.com/beta/me/findRooms(RoomList='" + map.get(buildingName) + "')";
        Log.d("TAG", roomsURL);
        callFindRoomListAPI(intent.getStringExtra("TOKEN"));
    }

        private void callFindRoomListAPI(final String token) {
            Log.d(TAG, "Microsoft Identity Platform Access Token " + token);
            MSGraphRequestWrapper.callGraphAPIUsingVolley(RoomsList.this,
                    roomsURL,
                    token,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, "Response: " + response.toString());
                            //ArrayAdapter<String> roomAdapter = new ArrayAdapter<>(RoomsList.this, android.R.layout.simple_list_item_1, roomsL);
                            //roomsListView.setAdapter(roomAdapter);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, "Error: " + error.toString());
                        }
                    });
        }
    }
