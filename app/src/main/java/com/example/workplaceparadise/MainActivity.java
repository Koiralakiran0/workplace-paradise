package com.example.workplaceparadise;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAccount;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.SilentAuthenticationCallback;
import com.microsoft.identity.client.exception.MsalClientException;
import com.microsoft.identity.client.exception.MsalException;
import com.microsoft.identity.client.exception.MsalServiceException;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Test";
    final String graphURL = "https://graph.microsoft.com/v1.0/me";

    private ISingleAccountPublicClientApplication mSingleAccountApp;

    Button buttonLogin;
    Button buttonLogOut;
    TextView textViewLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //INITIALIZATION
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogOut = findViewById(R.id.buttonLogOut);
        textViewLog = findViewById(R.id.textViewLog);

        PublicClientApplication.createSingleAccountPublicClientApplication(MainActivity.this, R.raw.auth_config_single_account,
                new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                        loadAccount();
                    }

                    @Override
                    public void onError(MsalException exception) {
                        displayError (exception);
                    }
                });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSingleAccountApp == null){
                    return;
                }
                String[] scopes = {"User.Read","Calendars.Read"};
                mSingleAccountApp.signIn(MainActivity.this, null, scopes, getAuthInteractiveCallback());
                //Toast.makeText(MainActivity.this, "Sign-In: Successful", Toast.LENGTH_SHORT).show();
            }
        });

        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSingleAccountApp == null){
                    return;
                }

                mSingleAccountApp.signOut(new ISingleAccountPublicClientApplication.SignOutCallback() {
                    @Override
                    public void onSignOut() {
                        updateUI(null);
                    }

                    @Override
                    public void onError(@NonNull MsalException exception) {
                        displayError(exception);
                    }
                });
            }
        });
    }


    private void displayError(MsalException exception) {
        Log.d(TAG, exception.toString());
    }

    private void loadAccount() {
        if (mSingleAccountApp == null) {
            return;
        }

        mSingleAccountApp.getCurrentAccountAsync(new ISingleAccountPublicClientApplication.CurrentAccountCallback() {
            @Override
            public void onAccountLoaded(@Nullable IAccount activeAccount) {
                updateUI(activeAccount);
            }

            @Override
            public void onAccountChanged(@Nullable IAccount priorAccount, @Nullable IAccount currentAccount) {
                updateUI(currentAccount);
            }

            @Override
            public void onError(@NonNull MsalException exception) {
                displayError(exception);
            }
        });
    }

    private void updateUI(@Nullable final IAccount account) {
        if (account != null){
            buttonLogin.setEnabled(false);
            buttonLogOut.setEnabled(true);
        } else { //clicking logout
            buttonLogin.setEnabled(true);
            buttonLogOut.setEnabled(false);
            textViewLog.setText(null);
        }
    }

    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onCancel() {
                Log.d(TAG, "User cancelled login.");
            }

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, use it to call a protected resource - MSGraph */
                Log.d(TAG, "Successfully authenticated");
                Log.d(TAG, "ID Token: " + authenticationResult.getAccount().getClaims().get("id_token"));
                updateUI(authenticationResult.getAccount());
                callGraphAPI(authenticationResult);

                //callMessageAPI(authenticationResult);
            }

            @Override
            public void onError(MsalException exception) {
                Log.d(TAG, "Authentication failed: " + exception.toString());
            }
        };
    }

    private void callGraphAPI(final IAuthenticationResult authenticationResult) {

        Log.d(TAG, "Microsoft Identity Platform Access Token " + authenticationResult.getAccessToken());
        MSGraphRequestWrapper.callGraphAPIUsingVolley(MainActivity.this,
                graphURL.toString(),
                authenticationResult.getAccessToken(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Response: " + response.toString());
                        showUserProfile(response, authenticationResult);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error: " + error.toString());
                    }
                });
    }


    private void showUserProfile(@NonNull final JSONObject response, final IAuthenticationResult authenticationResult) {
        Intent intent = new Intent(MainActivity.this, Profile.class);
        intent.putExtra("DATA", response.toString());
        intent.putExtra("TOKEN", authenticationResult.getAccessToken());
        startActivity(intent);
    }


}
